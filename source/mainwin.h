// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __MAINWIN_H
#define __MAINWIN_H


#include <clthreads.h>
#include <clxclient.h>
#include "global.h"
#include "splashwin.h"
#include "impdata.h"
#include "editwin.h"
#include "filewin.h"
#include "filtwin.h"
#include "progwin.h"
#include "exptwin.h"
#include "convolve.h"
#include "acweight.h"


class Mainwin : public X_window, public X_callback
{
public:

    Mainwin (X_window *parent, X_resman *resman, X_callback *callb, Filewin *filewin, ITC_ctrl *itcctrl);
    ~Mainwin (void);

    bool running (void) const { return _state != EXIT; }
    void stop (void);
    void handle_time (void);    
    void handle_mesg (ITC_mesg *);
    void handle_prog (void);
    void set_sess (const char *sess) { _sessdir = sess; }
    
private:

    enum { INIT, VIEW, CONV, EDIT, SINT, FILT, TERM, EXIT };
    enum { LMAR = 5, TMAR = 60 };
    enum
    {
      XPOS =  100, YPOS = 100,
      XMIN =  650, YMIN = 350,
      XDEF =  800, YDEF = 500
    };

    enum
    {
        B_SESS, B_LOAD, B_SAVE, B_IMPT, B_EXPT,
        B_SGEN, B_CAPT, B_CONV, B_EDIT, B_SINT, B_FILT,
        B_CANC, B_APPL, B_SONE, B_SALL,
        B_FILE,   
        B_TREF, B_GAIN, B_TRM0, B_TRM1, B_FADE,
	B_FRMI, B_FRPL, B_CHAN
    };

    virtual void handle_event (XEvent *);
    virtual void handle_callb (int, X_window *, XEvent *);

    void xcmesg (XClientMessageEvent *);
    void resize (XConfigureEvent *E);
    void makewin (void);
    void addtext (X_window *W, int xp, int yp, int xs, int ys, const char *text, int align);
    void askdata (X_textip *T);
    void ptime (int v, X_textip *T);
    void fileinfo (void);
    
    void settrace (int k, int b);
    void modif (X_window *W);
    void apply (void);
    void cancel (void);

    void next_sect (int d);
    void save_one (void);
    void save_all (void);

    void file_load (void);
    void file_save (void);
    void file_impt (void);
    void file_expt (void);

    void set_disp (Impdata *data, int nchan1, const char **labels);
    void set_view (void);
    void set_conv (void);
    void set_edit (void);
    void set_sint (void);
    void set_filt (void);

    void conv_init (void);
    int  conv_prep (M_calc_conv *);
    int  conv_save (const char *, bool);
    void conv_calc_one (const char *);
    void conv_done_one (M_calc_conv *);
    void conv_calc_all (const char *);
    void conv_done_all (M_calc_conv *);

    void edit_init (void);
    int  edit_prep (M_calc_edit *);
    int  edit_save (const char *, bool);
    void edit_calc_one (const char *);
    void edit_done_one (M_calc_edit *);
    void edit_calc_all (const char *);

    void edit_done_all (M_calc_edit *);
    void edit_tref (bool undo); 
    void edit_gain (bool undo, bool norm); 
    void edit_trim0 (bool undo); 
    void edit_trim1 (bool undo); 
    void edit_fade (bool undo); 

    void sint_init (void);
    void sint_exec (void);

    Atom            _xatom;
    X_callback     *_callb;
    int             _xp;
    int             _yp;
    int             _xs;
    int             _ys;
    int             _state;
    int             _count;
    int             _tmask;
    bool            _flock;
    bool            _doall;
    ITC_ctrl       *_itcctrl;
    Splashwin      *_splashw;
    Filewin        *_filewin;
    Exptwin        *_exptwin;
    Editwin        *_editwin;
    Filtwin        *_filtwin;
    Progwin        *_progwin;
    X_window       *_ifwview;
    X_window       *_ifwconv;
    X_window       *_ifwedit;
    X_window       *_ifwsint;
    X_window       *_ifwfilt;

    X_menuwin      *_mfile;
    X_button       *_bsess;
    X_button       *_bload;
    X_button       *_bsave;
    X_button       *_bimpt;
    X_button       *_bexpt;
    X_textip       *_tfile;

    X_button       *_bsgen;
    X_button       *_bcapt;
    X_button       *_bconv;
    X_button       *_bedit;
    X_button       *_bsint;
    X_button       *_bfilt;

    X_tbutton      *_bchan [4];
    X_button       *_bfrmi;
    X_button       *_bfrpl;
    X_textip       *_tfrag;

    X_window       *_modwin;
    Impdata        *_disp;
    char            _file1 [1024];
    char            _file2 [1024];
    char           *_base1;
    Impdata        *_data1;
    Impdata        *_data2;
    int             _nchan1;
    int             _cmask2;

    X_tbutton      *_bappl;
    X_textip       *_tcfil; 
    X_textip       *_tcfil1;
    X_textip       *_tcfil2;
    X_textip       *_tctim1;
    X_textip       *_tctim2;
    Convolver      *_convol;
    unsigned int    _convlen;

    X_tbutton      *_btref;
    X_tbutton      *_bgain;
    X_textip       *_ttref;
    X_textip       *_tgain;
    X_tbutton      *_btrim0;
    X_tbutton      *_btrim1;
    X_textip       *_ttrm00;
    X_textip       *_ttrm01;
    X_textip       *_ttrm10;
    X_textip       *_ttrm11;
    X_tbutton      *_bfade;
    X_textip       *_ttfade;
    X_textip       *_tslope;
    int             _offs;
    float           _gain;
    int             _ind0;
    int             _ind1;
    int             _trm00;
    int             _trm01;
    int             _trm10;
    int             _trm11;
    int             _tfade;
    float           _slope;

    ACweight       *_awfilt;
    int             _subs;

    int             _prog0;
    int             _prog1;

    const char     *_sessdir;

//    static X_menuwin_item _mfile_def []; 
};


#endif
