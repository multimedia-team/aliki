// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __MAINTHR_H
#define __MAINTHR_H


#include <clthreads.h>
#include "impdata.h"
#include "shdata.h"
#include "shmem.h"


class Mainthr : public A_thread
{
public:

    Mainthr (Shdata *shdata);
    ~Mainthr (void);
    virtual void thr_main (void);
    void stop (void);

private:

    enum { INIT, TERM, EXIT };
    enum { DONE = 1, TRIG = 2 };
    
    int  inittest (void);
    int  initcapt (void);
    int  iohandle (void);
    int  rdopdata (int nfrm);
    int  wripdata (int nfrm);
    void stopplay (void);
    void stopcapt (bool rm);
    
    int        _state;
    int        _count;
    Shdata    *_shdata;
    Impdata    _opfile;
    Impdata    _ipfile [8];
    int        _op_it;
    int        _op_si;
    int        _ip_it;
    int        _ip_si;
    float      _buff [8192];
};


#endif
