// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __GLOBAL_H
#define __GLOBAL_H


#include "convolve.h"
#include "impdata.h"


enum 
{
    TO_CALC    =  0,
    TO_CTRL    =  1,
    EV_X11     = 16,
    EV_PROG    = 17,
    EV_EXIT    = 31
};


enum
{
    CB_MAIN_CLOSE = 0x1000,
    CB_CAPT_AVAIL,
    CB_CAPT_NOTAV,
    CB_CAPT_READY,
    CB_SGEN_READY,
    CB_FILE_REQ,
    CB_FILE_CANC,
    CB_FILE_SESS,
    CB_FILE_LOAD,
    CB_FILE_SAVE,
    CB_FILE_IMPT,
    CB_FILE_EXPT,
    CB_OPEN_SGEN,
    CB_OPEN_CAPT
}; 


enum
{
    EXP_TYPE_RAW,
    EXP_TYPE_WAV,
    EXP_TYPE_WAVEX,
    EXP_TYPE_AMBIS
};


enum
{
    EXP_FORM_16B,
    EXP_FORM_24B,
    EXP_FORM_FLT
};


enum 
{
    EXP_CHAN_SEL,
    EXP_CHAN_ALL
};


enum
{
    EXP_SECT_CURR,
    EXP_SECT_COMB,
    EXP_SECT_ITER
};


enum 
{
    MT_CONV_ONE = 1,
    MT_CONV_ALL,
    MT_EDIT_ONE,
    MT_EDIT_ALL,
    MT_EXPORT
};


class M_calc_conv : public ITC_mesg
{
public:
    
    M_calc_conv (U32 type) : ITC_mesg (type) {}

    Impdata    *_data1;
    Impdata    *_data2;
    int         _sect;
    Convolver  *_conv;
    int         _k0;
    int         _k1;
};


class M_calc_edit : public ITC_mesg
{
public:
    
    M_calc_edit (U32 type) : ITC_mesg (type) {}

    Impdata    *_data1;
    Impdata    *_data2;
    int         _sect;
    int         _offs;
    float       _gain;
    int         _ind0;
    int         _ind1;
    int         _trm00;
    int         _trm01;
    int         _trm10;
    int         _trm11;
    int         _tfade;
    float       _slope;
};


class M_export : public ITC_mesg
{
public:
    
    M_export (void) : ITC_mesg (MT_EXPORT) {}

    Impdata    *_impd;
    const char *_name;
    int         _type;
    int         _form;
    int         _sect;
    U32         _mask;
    int         _rval;
};


#endif

