// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __CALCTHR_H
#define __CALCTHR_H


#include <clthreads.h>
#include <sndfile.h>
#include "global.h"


class Calcthr : public A_thread
{
public:

    Calcthr (void);
    ~Calcthr (void);
    virtual void thr_main (void);
    void stop (void);

private:

    void handle_messsage (ITC_mesg *M);
    void conv_one (M_calc_conv *M);
    void conv_all (M_calc_conv *M);
    void edit_one (M_calc_edit *M);
    void edit_all (M_calc_edit *M);
    void exp_impdata (M_export *M);
    SNDFILE *exp_open_multi  (Impdata *D, const char *name, U32 type, U32 form, S32 sect, U32 mask);
    SNDFILE *exp_open_single (Impdata *D, const char *name, U32 type, U32 form, S32 sect, U32 chan);
    int exp_writesect (SNDFILE *S, Impdata *D, U32 mask, U32 chan, float *data);
};


#endif
