// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include "styles.h"
#include "splashwin.h"



Splashwin::Splashwin (X_window *parent, int xp, int yp, int xs, int ys) :
    X_window (parent, xp, yp, xs, ys, Colors.spla_bg, Colors.spla_bd, 1),
    _xs (xs), _ys (ys)
{
    x_add_events (ExposureMask); 
}


Splashwin::~Splashwin (void)
{
}


void Splashwin::handle_event (XEvent *E)
{
    switch (E->type)
    {
    case Expose:
        expose ((XExposeEvent *) E);
        break;  
    }
}


void Splashwin::expose (XExposeEvent *E)
{
    int     x, y;
    char    s [256];
    X_draw  D (dpy (), win (), dgc (), xft ());

    if (E->count) return;
    x = _xs / 2;
    y = _ys / 2;
    sprintf (s, "Aliki-%s", VERSION);
    D.setfunc (GXcopy);
    D.setfont (XftFonts.spla1);
    D.setcolor (XftColors.spla_fg);
    D.move (x, y - 50);
    D.drawstring (s, 0); 
    D.setfont (XftFonts.spla2);
    D.move (x, y);
    D.drawstring ("(C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>", 0); 
    D.move (x, y + 50);
    D.drawstring ("This is free software, and you are welcome", 0);
    D.move (x, y + 70);
    D.drawstring ("to distribute it under certain conditions.", 0); 
    D.move (x, y + 90);
    D.drawstring ("See the file COPYING for details.", 0); 
}



