// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __ITYPES_H
#define __ITYPES_H


#include <inttypes.h>


typedef uint16_t         U16; // Unsigned 16-bit
typedef int16_t	 	 S16; // Signed 16-bit
typedef uint32_t         U32; // Unsigned 32-bit
typedef int32_t	 	 S32; // Signed 32-bit
typedef uint64_t         U64; // Unsigned 64-bit
typedef int64_t	 	 S64; // Signed 64-bit


#endif
