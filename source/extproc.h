// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __EXTPROC_H
#define __EXTPROC_H


#include "shmem.h"


class Extproc
{
public:

    Extproc (const char *shmname, size_t shmsize);
    ~Extproc (void);

    const char  *shmname (void) const { return _shmname; }
    void        *shmdata (void) const { return _shmem->data (); }
    size_t       shmsize (void) const { return _shmem->size (); }    
    int          procid (void)  const { return _procid; } 
    int          procrc (void)  const { return _procrc; } 

    int  start (const char *prefix, const char *procfile);
    int  kill (void);
    int  wait (void);

private:

    char       _shmname [64];
    char      *_args [4];
    Shmem     *_shmem;
    int        _procid;
    int        _procrc;
};


#endif

