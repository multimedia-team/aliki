// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __CONTROL_H
#define __CONTROL_H


#include <clthreads.h>
#include <clxclient.h>
#include "mainwin.h"
#include "captwin.h"
#include "filewin.h"
#include "sgenwin.h"


class Control : public A_thread, public X_callback
{
public:

    Control (X_display *display, X_resman *resman);
    ~Control (void);

    virtual void thr_main (void);

    void stop (void);

private:

    enum { INIT, TERM };

    virtual void handle_callb (int, X_window *, XEvent *);
    
    void init (void);
    void fini (void);
    void handle_event (int);
    void handle_mesg (ITC_mesg *);     
    void handle_prog (void);     
    void handle_time (void);     
    void makesess (void);

    X_display  *_display;
    X_resman   *_resman;
    X_handler  *_xhandler;
    X_rootwin  *_rootwin;
    Mainwin    *_mainwin;
    Editwin    *_editwin;
    Captwin    *_captwin;
    Filewin    *_filewin;
    Sgenwin    *_sgenwin;
    
    char        _sessiondir [1024];
    char        _sweepfile [1024];
    char        _fcorrfile [1024];
};



#endif
