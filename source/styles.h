// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __STYLES_H
#define __STYLES_H


#include "clxclient.h"


struct colors
{
    unsigned long   white;
    unsigned long   black;
    unsigned long   main_ds;
    unsigned long   main_ls;
    unsigned long   text_bg;
    unsigned long   text_hl;
    unsigned long   text_mk;
    unsigned long   spla_bg;
    unsigned long   spla_bd;
    unsigned long   plot_bg;
    unsigned long   plot_gr;
    unsigned long   plot_da [2];
    unsigned long   plot_mk [2];
    unsigned long   meter_bg;
    unsigned long   meter_ds;
    unsigned long   meter_ls;
    unsigned long   meter_offc0;
    unsigned long   meter_offc1;
    unsigned long   meter_offmk;
    unsigned long   meter_actc0;
    unsigned long   meter_actc1;
    unsigned long   meter_actmk;
};


struct fonts 
{
};


struct xft_colors
{
    XftColor  *white;
    XftColor  *black;
    XftColor  *main_bg;
    XftColor  *main_fg;
    XftColor  *meter_fg;
    XftColor  *tick0;
    XftColor  *tick1;
    XftColor  *spla_fg;
    XftColor  *text_fg;
    XftColor  *spect_fg;
    XftColor  *spect_sc;
    XftColor  *butt_bg [4];
    XftColor  *butt_fg [4];
    XftColor  *menu_bg;
    XftColor  *menu_fg [4];
    XftColor  *menu_mm;
    XftColor  *mark_fg;
    XftColor  *plot_sc;
};


struct xft_fonts 
{
    XftFont   *spla1;
    XftFont   *spla2;
    XftFont   *button;
    XftFont   *labels;
    XftFont   *scales;
    XftFont   *files;
    XftFont   *menus;
};



extern struct colors       Colors;
extern struct fonts        Fonts;
extern struct xft_colors   XftColors;
extern struct xft_fonts    XftFonts;
extern X_button_style      Bst0, Bst1, Bst2;
extern X_textln_style      Tst0, Tst1;
extern X_menuwin_style     Mst1, Mst2; 
extern X_mclist_style      Lst0; 
extern X_scale_style       Slevel, Sprogr;
extern X_meter_style       Mlevel, Mprogr; 


extern void init_styles (X_display *disp, X_resman *xrm);


#define PROGNAME  "Aliki"


#endif
