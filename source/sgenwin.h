// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __SGENWIN_H
#define __SGENWIN_H


#include <clxclient.h>
#include "impdata.h"



class Sgenwin : public X_window , public X_callback
{
public:

    Sgenwin (X_window *parw, X_resman *resman, X_callback *callb);
    ~Sgenwin (void);

    void hide (void);
    void show (void);
    void set_sess (const char *sess) { _sess = sess; }
    void set_rate (int rate) { _rate = rate; }
 
private:

    enum
    {
	XDEF = 420, YDEF = 180,
	XPOS = 150, YPOS = 150
    };

    enum { B_LIN, B_LOG, B_CANC, B_SGEN };

    virtual void handle_event (XEvent *xe);
    virtual void handle_callb (int, X_window*, _XEvent*);

    void xcmesg (XClientMessageEvent *E);
    void generate (void);
    void add_text (int xp, int yp, int xs, int ys, const char *text, int align);
    void askdata (X_textip *T);
    void gensweep (void);
    void genlogsweep (void);
    void genlinsweep (void);

    Atom         _xatom;
    X_callback  *_callb;

    X_textip    *_ready;
    X_textip    *_tname;
    X_textip    *_trate;
    X_textip    *_ttim1;
    X_textip    *_ttim2;
    X_textip    *_ttim3;
    X_textip    *_tfmin;
    X_textip    *_tfmax;
    X_tbutton   *_blin;
    X_tbutton   *_blog;
    X_tbutton   *_bcanc;
    X_tbutton   *_bsgen;

    const char  *_sess;
    int          _rate;
    int          _k0;
    int          _k1;
    int          _k2;
    float        _fmin;
    float        _fmax;
    Impdata      _impd;
};


#endif
