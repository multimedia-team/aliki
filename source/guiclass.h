// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __GUICLASS_H
#define __GUICLASS_H

#include <clxclient.h>


class Radiosel : public X_callback
{
public:

    Radiosel (X_window *parw, X_callback *xcbh);
    ~Radiosel (void);
    
    void init (X_button_style *bst, X_textln_style *tst, const char **text,
               int nbut, int xpos, int ypos, int xsize, int ystep, int cbid);

    void set_stat (int stat);
    int  stat (void) const { return _stat; }

private:

    void handle_callb (int k, X_window *W, _XEvent *E);

    int              _nbut;
    int              _stat;
    int              _cbid;
    X_button       **_butt;  
    X_window        *_parw;
    X_callback      *_xcbh;
};


#endif
