// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __SHDATA_H
#define __SHDATA_H


#include <limits.h>
#include "itypes.h"
#include "lfqueue.h"


#define SHM_SIZE 4096


class Evport
{
public:

    Evport (void) : _txe (0), _rxe (0) {}

    void eput (U32 e)
    {
	e &= ~(_txe ^ _rxe);
        _txe ^= e; 
    }

    U32 eget (U32 m = ~0)
    {
        m &= (_txe ^ _rxe);
        m ^= m & (m - 1);
        _rxe ^= m;
        return m;  
    }

private:

    U32   _txe;
    U32   _rxe;
};


class Ipdata
{
public:

    char     _name [64];
    U32      _stat;
    U32      _type;
    U32      _chan0;
    Fltlfq  *_queue;    
};   


class Opdata
{
public:

    char     _name [1024];
    U32      _stat;
    U32      _chan;
    Fltlfq  *_queue;    
};   


class Shdata
{
public:

    enum { X_EXIT = 1, X_INIT = 2, X_TICK = 4, X_STOP = 8, X_TEST = 16, X_CAPT = 32, X_TRIG = 64 };
    enum { A_JACK = 1, A_ALSA = 2, A_NORT = 4, A_FAIL = 8, A_KILL = 16, A_SIZE = 32 };
    enum { M_PLAY = 1, M_MEAS = 2, M_CAPT = 4 };
    enum { S_NONE, S_OPEN, S_ERROR };

    // Versions
    U32      _pversion;      
    U32      _cversion;     
  
    // Event ports
    Evport   _par2cld;
    Evport   _cld2par;

    // Audio driver params
    U32      _fsamp;
    U32      _nplay;
    U32      _ncapt;
    U32      _fsize;
    U32      _nfrag;
    S32      _class;
    S32      _prior;
    U32      _state;
    char     _ident [64];

    // Audio work params
    U32      _mode;
    U32      _opsel;
    S32      _niter;
    S32      _itlen; 
    S32      _oplen;
    S32      _iplen;
    float    _level;
    Opdata   _opdat;    
    Ipdata   _ipdat [8];
    float    _meter [8];    
    char     _error [96];

    // Session path
    char     _sessdir [1024];
};


#endif

