// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include "control.h"
#include "global.h"


Control::Control (X_display *display, X_resman *resman) :
    A_thread ("Control"),
    _display (display),
    _resman (resman),
    _mainwin (0),
    _captwin (0),
    _filewin (0),
    _sgenwin (0)
{
    _rootwin = new X_rootwin (display);
    _xhandler = new X_handler (display, this, EV_X11);
    _xhandler->next_event ();

    strcpy (_sessiondir, ".");
    strcpy (_sweepfile, "");
}


Control::~Control (void)
{
    delete _xhandler;
    delete _rootwin;
}


void Control::stop (void)
{
    _mainwin->stop ();
    _captwin->stop ();
}


void Control::init (void)
{
    const char *p;

    _filewin = new Filewin (_rootwin, _resman);
    _mainwin = new Mainwin (_rootwin, _resman, this, _filewin, this);
    _captwin = new Captwin (_rootwin, _resman, this, _filewin);
    _sgenwin = new Sgenwin (_rootwin, _resman, this);
    p = getenv ("HOME");
    _filewin->set_home (p ? p : ".");
}


void Control::fini (void)
{
    delete _mainwin;
    delete _captwin;
    delete _filewin;
    delete _sgenwin;
}


void Control::thr_main (void)
{
    init ();
    set_time ();
    inc_time (100000);
    while (_mainwin->running () || _captwin->running ())
    {
	handle_event (get_event_timed ());
    }
    fini ();
}


void Control::handle_event (int k)
{
    switch (k)
    {
    case TO_CTRL:
	handle_mesg (get_message ());
	break;

    case EV_X11:
	_rootwin->handle_event ();
        _xhandler->next_event ();
	break;

    case EV_PROG:
	handle_prog ();
	break;

    case EV_TIME:
        inc_time (50000);
	handle_time ();
        XFlush (_display->dpy ());
        break;

    default:
	;
    }        
}


void Control::handle_mesg (ITC_mesg *M)
{
    _mainwin->handle_mesg (M);
}


void Control::handle_prog (void)
{
    _mainwin->handle_prog ();
}


void Control::handle_time (void)
{
    _mainwin->handle_time ();
    _captwin->handle_time ();
}


void Control::handle_callb (int k, X_window *W, XEvent *E)
{
    if (k == (BUTTON | X_button::RELSE)) k = ((X_button *) W)->cbid ();

    switch (k)
    {
    case CB_MAIN_CLOSE:
        stop ();
	break;

    case CB_CAPT_AVAIL:
        _sgenwin->set_rate (_captwin->get_rate ()); 
        _captwin->set_sess (_sessiondir);
	break;

    case CB_CAPT_NOTAV:
        _captwin->stop ();
        _captwin->hide ();
	break;

    case CB_CAPT_READY:
	break;

    case CB_SGEN_READY:
	break;

    case CB_OPEN_SGEN:
        _sgenwin->show ();
	break;

    case CB_OPEN_CAPT:
        _captwin->show ();
	break;

    case CB_FILE_SESS:
	makesess ();
	break;

    default:
	;
    }
}


void Control::makesess (void)
{
    strcpy (_sessiondir, _filewin->wdir ());
    chdir (_sessiondir);
    mkdir ("sweep", 0777);
    mkdir ("capture", 0777);
    mkdir ("impresp", 0777);
    mkdir ("edited", 0777);
    _filewin->set_sess (_sessiondir);
    _mainwin->set_sess (_sessiondir);
    _sgenwin->set_sess (_sessiondir);
    _captwin->set_sess (_sessiondir);
}

