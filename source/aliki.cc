// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <clthreads.h>
#include <signal.h>
#include "styles.h"
#include "control.h"
#include "calcthr.h"


#define NOPTS 8

#define CP (char *)

XrmOptionDescRec options [NOPTS] =
{
    {CP"-h",   CP".help",      XrmoptionNoArg,  CP"true"  },
    {CP"-J",   CP".jack",      XrmoptionNoArg,  CP"true"  },
    {CP"-A",   CP".alsa",      XrmoptionNoArg,  CP"true"  },
    {CP"-E",   CP".rtexec",    XrmoptionSepArg,  0      }, 
    {CP"-d",   CP".device",    XrmoptionSepArg,  0      },
    {CP"-r",   CP".fsamp",     XrmoptionSepArg,  0      },
    {CP"-p",   CP".period",    XrmoptionSepArg,  0      },
    {CP"-n",   CP".nfrags",    XrmoptionSepArg,  0      }
};


static void help (void)
{
    fprintf (stderr, "Aliki-%s\n  (C) Fons Adriaensen\n  <fons@linuxaudio.org>\n\n", VERSION);
    fprintf (stderr, "-h                 Display this text\n");
    fprintf (stderr, "-J                 Use JACK\n");
    fprintf (stderr, "-A                 Use ALSA, with options:\n");
    fprintf (stderr, "  -d <device>        Alsa device [hw:0,0]\n");
    fprintf (stderr, "  -r <rate>          Sample frequency [48000]\n");
    fprintf (stderr, "  -p <period>        Period size [1024]\n");
    fprintf (stderr, "  -n <nfrags>        Number of fragments [2]\n");
    fprintf (stderr, "-E <prefix>          Prefix for audio process, e.g. sudo\n\n");
    fprintf (stderr, "Without either -J or -A only the file processing functions are available.\n\n");
    exit (1);
}


static X_resman  resman;
static Control  *control;
static Calcthr  *calcthr;


static void sigint_handler (int)
{
    signal (SIGINT, SIG_IGN);
    control->stop ();
}


int main (int argc, char *argv [])
{
    X_display  *display;

    resman.init (&argc, argv, CP"aliki", options, NOPTS);
    if (resman.getb (".help", 0)) help ();
            
    display = new X_display (resman.get (".display", 0));
    if (display->dpy () == 0)
    {
	fprintf (stderr, "Can't open display !\n");
        delete display;
	exit (1);
    }

    init_styles (display, &resman);
    control = new Control (display, &resman);
    calcthr = new Calcthr ();
    calcthr->thr_start (SCHED_OTHER, 0, 0x00010000);

     ITC_ctrl::connect (control, TO_CALC, calcthr, TO_CALC);
     ITC_ctrl::connect (calcthr, TO_CTRL, control, TO_CTRL);
     ITC_ctrl::connect (calcthr, EV_PROG, control, EV_PROG);

    signal (SIGINT,  sigint_handler); 
    signal (SIGFPE,  sigint_handler); 
    signal (SIGSEGV, sigint_handler); 

    control->thr_main ();

    delete control;
    delete display;
   
    return 0;
}



