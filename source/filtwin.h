// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __FILTWIN_H
#define __FILTWIN_H


#include <clxclient.h>
#include "impdata.h"


class Dots
{
public:

    int v2y (float v)
    {
        return _y0 - (int)(v * (_y0 - _y1) + 0.5f);
    }

    float y2v (int y)
    {
        if (y > _y0) return 0.0f;
        if (y < _y1) return 1.0f;
        return  (float)(_y0 - y) / (_y0 - _y1);
    }

    unsigned long _col;
    int           _n;
    int           _y0;
    int           _y1;
    float         _v [33];
};
    

class Filtwin : public X_window, public X_callback
{
public:

    Filtwin (X_window *parent, X_callback *callb, int xp, int yp, int xs, int ys);
    ~Filtwin (void);

    void resize (int xs, int ys);
    X_window *bbwin (void) { return _bbwin; }

private:

    enum { LSW = 40, RSW = 40, TPM = 5, BSH = 22, BWH = 25 };

    virtual void handle_event (XEvent *);
    virtual void handle_callb (int k, X_window *W, XEvent *E );

    void expose (XExposeEvent *);
    void bpress (XButtonEvent *);
    void motion (XPointerMovedEvent *);
    void brelse (XButtonEvent *);
    void set_xscale (void);
    void set_yscale (void);
    void plot_xscale (void);
    void plot_lscale (void);
    void plot_rscale (void);
    void update (void);
    void redraw (void);
    void plot_grid (X_draw *D);
    void plot_data (X_draw *D);
    void plot_dots (X_draw *D, Dots *P);
    void finddot (int x);
    void movedot (int y);

    X_callback     *_callb;
    int             _xs;
    int             _ys;
    int             _xd;
    int             _yd;
    bool            _show;
    bool            _wait;
    X_window       *_plotw;
    X_window       *_lswin;
    X_window       *_rswin;
    X_window       *_bswin;
    X_window       *_bbwin;
    int             _xticks [33];
    int             _yticks [9];
    float           _xunit;
    float           _yunit;
    Dots            _dots1;
    Dots            _dots2;
    Dots           *_dots;   
    int             _idot;

    static const char *_xtexts [33];
};


#endif
