// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __EDITWIN_H
#define __EDITWIN_H


#include <clxclient.h>
#include "impdata.h"


class Mark
{
public:

    enum { OFF, ON, LL, RR };

    int            _flags;
    int            _posit;
    unsigned long  _color;
    X_ibutton     *_butt;
};


class Editwin : public X_window, public X_callback
{
public:

    Editwin (X_window *parent, X_callback *callb, int xp, int yp, int xs, int ys);
    ~Editwin (void);

    enum { LOGMODE = 1, POWMODE = 2, OVERLAY = 4 };

    X_window *bbwin (void) { return _bbwin; }
    X_window *rbwin (void) { return _rbwin; }

    void set_data (Impdata *I, int mask, int opts, float t0, float t1);
    void set_mask (int mask);
    int  get_mask (void) { return _mask; }
    void set_xrange (float t0, float t1);
    void set_xzoom (int);
    void set_yzoom (int);
    void set_offs (int);
    void set_ind0 (int);
    void set_gain (float);
    void unset_all (void);
    void logmode (bool);
    void overlay (bool);
    void update (void);
    void resize (int xs, int ys);
    void clear_marks (void);
    int get_currmark (void) const { return _cmark; }
    int get_markpos (int i) const { return _marks [i]._flags ? _marks [i]._posit : INT_MIN; }
    void addtext (X_window *W, int xp, int yp, int xs, int ys, const char *text, int align);

private:

    enum { LSW = 36, RPM = 6, RWW = 37, TPM = 5, BSH = 22, BWH = 21 };
    enum { NZOOM = 22, NYTAB = 12, MOVEX = 1, ZOOMX = 2, ZOOMY = 3  };
    enum { NTRACE = 12, NMARK = 2 };
    enum { B_XMI, B_XPL, B_LIN, B_LOG, B_YPL, B_YMI, B_MK0, B_MK1 };

    virtual void handle_event (XEvent *);
    virtual void handle_callb (int k, X_window *W, XEvent *E );

    void expose (XExposeEvent *);
    void bpress (XButtonEvent *);
    void motion (XPointerMovedEvent *);
    void brelse (XButtonEvent *);
    void set_xscale (void);
    void set_yscale (void);
    void plot_xscale (void);
    void plot_yscale (void);
    void xscroll (int dx);
    void redraw (void);
    void plot_grid (X_draw *D);
    void plot_data (X_draw *D);
    void check_marks (void);
    void show_mark (int i, int x);
    void move_mark (int x);
    void goto_mark (int i);
    int  find_mark (int x, int y);
    void togg_mark (int i);
    void curr_mark (int i);
    void plot_mark (X_draw *D, int i);

    X_callback     *_callb;
    int             _xs;
    int             _ys;
    int             _xd;
    int             _yd;
    int             _x0;
    int             _x1;
    bool            _show;
    bool            _wait;
    X_window       *_plotw;
    X_window       *_lswin;
    X_window       *_bswin;
    X_window       *_rbwin;
    X_window       *_bbwin;
    X_tbutton      *_blin;
    X_tbutton      *_blog;
    X_ibutton      *_bxmi;
    X_ibutton      *_bxpl;
    X_ibutton      *_bypl;
    X_ibutton      *_bymi;
    X_textip       *_tmark;
    XSegment       *_segm;
    Impdata        *_data;
    int             _rate;
    int             _fst0;
    int             _offs;
    int             _ind0; 
    float           _gain;
    int             _zoom;
    int             _zind;
    int             _xi0;
    int             _xmm;
    double          _xun;
    double          _xsc;
    int             _mask;
    int             _opts;
    int             _yref;
    int             _ydel;
    int             _ypos [NTRACE];
    int             _slin;
    int             _slog;
    int             _xdrag;
    int             _mdrag;
    int             _cmark;
    Mark            _marks [NMARK];

    static int _ztab [NZOOM];
    static int _ytab [NYTAB];
};


#endif
